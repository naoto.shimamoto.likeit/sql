﻿SELECT t.category_name,SUM(m.item_price)AS total_price 
FROM item m INNER JOIN item_category t  ON m.category_id=t.category_id
GROUP BY t.category_name
ORDER BY total_price DESC;
